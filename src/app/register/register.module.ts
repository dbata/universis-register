import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewComponent } from './components/new/new.component';
import { RegisterRoutingModule } from './register-routing.module';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TranslateModule } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { ListComponent } from './components/list/list.component';
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';
import { SelectStudyProgramComponent } from './components/select-study-program/select-study-program.component';
@NgModule({
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FormsModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    TabsModule,
    NgArrayPipesModule,
    NgxDropzoneModule
  ],
  declarations: [NewComponent, ListComponent, ComposeMessageComponent, SelectStudyProgramComponent]
})
export class RegisterModule { }
